package main

import (
	"encoding/json"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/levenlabs/go-llog"
)

// moringaConfig will hold the pointer the config file
type moringaConfig struct {
	file   fs.FileInfo
	config *configValues
}

type configValues struct {
	token    string  `json:"token"`
	groups   []asset `json:"groups"`
	projects []asset `json:"projects"`
}

type asset struct {
	name string `json:"name"`
}

// Configure returns a pointer to a configValues instance.
// On startup this gets called, and looks for a configuration file
// if one does not exist, then it will ask for user inputs to create one
func Configure() *moringaConfig {
	return &moringaConfig{}
}

func (m *moringaConfig) FindConfiguration() fs.FileInfo {
	// get the user's home directory
	homeDir, err := os.UserHomeDir()
	if err != nil {
		llog.ErrWithKV(err)
	}
	// check if environment variable exists
	// if not, set to user's home directory
	// if the this env variable is set then we want to look for the config in
	// directory specified
	configDir := os.Getenv("MORINGA_CONFIG_DIR")
	if configDir == "" {
		if file, err := os.Stat(filepath.Join(homeDir + "/.config/moringa/.moringa.json")); err == nil {
			return file
		}
		return nil
	}
	if f, err := os.Stat(filepath.Join(configDir, "/.moringa.json")); err == nil {
		return f
	}
	return nil
}

// TODO: func parseConfiguration
func (m *moringaConfig) LoadConfiguration() error {
	// Find if Configuration exists
	// If no config file, create one
	// and "load" that

	return errNotImplemented
}

func createConfiguration() {
}

func (m *moringaConfig) WriteConfigurationFile(path string) error {
	file, err := json.MarshalIndent(*m, "", "    ")
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filepath.Join(path, ".moringa.json"), file, 644)
	if err != nil {
		return err
	}

	return err
}

// TODO: ask user for inputs
func gatherUserInfo() error {
	return errNotImplemented
}
