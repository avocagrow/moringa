package main

import (
	"crypto/rand"
	"encoding/hex"
)

// RandStr returns a random alphanumeric string of an arbitrary length
func randomStr(size int) string {
	b := make([]byte, size)
	// this should never be needed but if it does, then we should find a nuclear shelter
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}
	return hex.EncodeToString(b)
}
