package main

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/levenlabs/go-llog"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	configPriority1 = "/.config/moringa/.moringa.json"
	configPriority2 = "/.config/.moringa.json"
	configPriority3 = "/.moringa.json"
	ogTmpDir        = os.Getenv("TMPDIR")
)

func setUp() {
	dir, err := os.Getwd()
	if err != nil {
		// this should never trip, but if it does then we are kinda screwed
		llog.Fatal("unable to get current working directory")
	}
	tmpDir := filepath.Join(dir, randomStr(6))
	os.Setenv("TMPDIR", tmpDir)
	os.Setenv("MORINGA_CONFIG_DIR", os.TempDir())
	os.Setenv("HOME", tmpDir)

	configPriority1 = "/.config/moringa/.moringa.json"
	err = os.MkdirAll(filepath.Join(os.TempDir(), ".config/moringa/"), os.ModePerm)
	if err != nil {
		llog.Fatal("unable to create tmp dirs for tests")
	}
}

func tearDown() {
	err := os.RemoveAll(os.TempDir())
	if err != nil {
		llog.Fatal("unable to remove everything in the temp dir")
	}
	os.Setenv("TMPDIR", ogTmpDir)
}

func TestFindConfiguration(t *testing.T) {
	setUp()

	// create the test file for first set of tests
	testMoringa := &moringaConfig{}
	expectedFilePath := filepath.Join(os.TempDir(), configPriority1)
	_, err := os.Create(expectedFilePath)
	require.NoError(t, err, llog.ErrKV(err))
	expected, err := os.Stat(expectedFilePath)
	require.NoError(t, err, llog.ErrKV(err))

	// if CONFIG_DIR env is not set and file exists
	os.Setenv("MORINGA_CONFIG_DIR", "")
	got := testMoringa.FindConfiguration()
	require.NotNil(t, got)
	assert.EqualValuesf(t, expected, got, "Expected to get %v, instead got %v", expected, got)
	err = os.Remove(filepath.Join(os.TempDir() + configPriority1))
	require.NoError(t, err, llog.ErrKV(err))
	// CONFIG_DIR is not set and file does not exist
	got = testMoringa.FindConfiguration()
	require.Nilf(t, got, "Expected to get nil, instead got %v", got)

	// set up tmp directory and set the config directory
	randomDir := filepath.Join(os.TempDir(), randomStr(6))
	err = os.Mkdir(randomDir, os.ModePerm)
	require.NoError(t, err, llog.ErrWithKV(err))
	os.Setenv("MORINGA_CONFIG_DIR", randomDir)
	// CONFIG_DIR is set, and file does not exist
	got = testMoringa.FindConfiguration()
	require.Nilf(t, got, "Expected to get nil, instead got %v", got)

	// create the test file in config_dir
	expectedFilePath = filepath.Join(randomDir, ".moringa.json")
	_, err = os.Create(expectedFilePath)
	require.NoError(t, err, llog.ErrWithKV(err))
	expected, err = os.Stat(expectedFilePath)
	require.NoError(t, err, llog.ErrWithKV(err))

	// CONFIG_DIR is set, and file exists
	got = testMoringa.FindConfiguration()
	require.NotNil(t, got)
	assert.EqualValuesf(t, expected, got, "Expected to get %v, instead got %v", expected, got)
	// clean up our act
	tearDown()
}

// TODO: TestCreateConfiguration
func TestCreateConfiguration(t *testing.T) {
}

// TODO: ask user for inputs
func TestGatherInfo(t *testing.T) {
}
